<h1>DWM- minimalist dynamic window manager on Slackware</h1>


![2022-02-22-110001_1920x1080_scrot](/uploads/1a7cd848e50cd0b35958f3da411c865b/2022-02-22-110001_1920x1080_scrot.png)

**Patches:**

[focusonnetactive](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-focusonnetactive-6.2.diff) <br>
[hide vacant tags](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-hide_vacant_tags-6.3.diff) <br>
[moveresize](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-moveresize-20210823-a786211.diff) <br>
[dwm-shiftclient-non-vacant-tags](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-shiftclient-non-vacant-tags-6.3.diff) <br>
[status2d](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-status2d-6.3.diff) <br>
[togglefloatingcenter](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-togglefloatingcenter-20210806-138b405f.diff) <br>
[xrdb](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-xrdb-6.2.diff) <br>
[vanitygaps](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-vanitygaps-20200610-f09418b.diff) <br>
[attachaside](https://gitlab.com/slackernetuk/dwm-slacker/-/raw/main/patches/dwm-attachaside-6.3.diff)

