#!/bin/bash

for i in \
../patches/dwm-focusonnetactive-6.2.diff \
../patches/dwm-hide_vacant_tags-6.3.diff \
../patches/dwm-moveresize-20210823-a786211.diff \
../patches/dwm-shiftclient-non-vacant-tags-6.3.diff \
../patches/dwm-status2d-6.3.diff \
../patches/dwm-togglefloatingcenter-20210806-138b405f.diff \
../patches/dwm-vanitygaps-20200610-f09418b.diff \
../patches/dwm-xrdb-6.2.diff \
../patches/dwm-attachaside-6.3.diff \
; do
patch -p1 < $i || exit 1
done
